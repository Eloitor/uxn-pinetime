# Uxn for smartwatches

Attempt to port [Uxn](https://100r.co/site/uxn.html)/[Varvara](https://wiki.xxiivv.com/site/varvara.html) for the pinetime and other smartwatches.
Since I only have the pinetime, for now I'll focus on it.

# Setup the emulator and uxnasm

1. Install [uxnasm](https://git.sr.ht/~rabbits/uxn)

2. Setup this repo

```bash
git clone https://codeberg.org/eloitor/uxn-pinetime
make submodules
make softdevice
```

3. Compile some `*.tal` code into `main.rom`, for example

```bash
uxnasm hilbert.tal hilbert.rom
```

4. Run the `main.rom`

```
make sim
```

## Roadmap
- Get Uxn machine working in the simulator: mostly done. Needs testing and optimization.
- Get Varvara VM workin in the simulator
  - Write on console: Works. Needs optimization
  - Draw pixel: Works. Needs testing and optimization
  - Draw sprites
  - Touch: Queued
  - Button: Queued
  - Date/Time: Works
  - Filesystem
- Write Watch-OS based on Varvara:
  - Watchface: Queued
  - Port apps form wasp-os / Infinitime
- Test in device with micropython, with 8k of RAM allocated for running uxn.
- Write a uxn emulator for the pinetime with C/C++/... to be able to allocate more RAM for UXN.

