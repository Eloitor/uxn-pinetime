# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright (C) 2020 Daniel Thompson
import sys
import wasp
import time

# # Increase the display blanking time to avoid spamming the console
# # with backlight activations.
# wasp.system.blank_after = 300

# wasp.system.run()

class StackOverflow(Exception):
    pass

class StackUnderflow(Exception):
    pass

def signed(u8):
    if u8 & 0x80:
        return -(0x100 - u8)
    else:
        return u8

class Stack:
    def __init__(self):
        self.array = bytearray(0x100)
        self.view = memoryview(self.array)
        self.ptr = 0
        self.kptr = 0
        self.k = False

    def push8(self, v):
        if self.ptr >= 0xff:
            raise StackOverflow()
        self.view[self.ptr] = v & 0xff
        self.ptr += 1

    def push16(self, v):
        if self.ptr >= 0xfe:
            raise StackOverflow()
        self.view[self.ptr] = (v & 0xff00) >> 8
        self.view[self.ptr+1] = v & 0xff
        self.ptr += 2

    def pop8(self):
        ptr = self.kptr if self.k else self.ptr
        if ptr < 1:
            raise StackUnderflow()
        ptr -= 1
        v = self.view[ptr]
        if self.k:
            self.kptr = ptr
        else:
            self.ptr = ptr
        return v

    def pop16(self):
        ptr = self.kptr if self.k else self.ptr
        if ptr < 2:
            raise StackUnderflow()
        ptr -= 2
        v = self.view[ptr] << 8
        v |= self.view[ptr+1]
        if self.k:
            self.kptr = ptr
        else:
            self.ptr = ptr
        return v

    def keep(self, mode):
        self.k = mode
        self.kptr = self.ptr

    def __repr__(self):
        return str([hex(self.array[i]) for i in range(self.ptr)])

class Uxn:
    page_program = 0x100
    def __init__(self):
        self.ram_array = bytearray(0x10000)
        self.ram = memoryview(self.ram_array)
        self.wst = Stack()
        self.rst = Stack()
        self.dev_array = bytearray(0x100)
        self.dev = memoryview(self.dev_array)
        self.pc = 0
        self.halted = False
        self.s = False
        self.r = False
        self.k = False

    def load(self, rom):
        self.ram[Uxn.page_program:Uxn.page_program + len(rom)] = rom

    def eval(self, pc):
        self.halted = False
        self.pc = pc
        while not self.halted:
            self.step()

    def set_pc(self, pc):
        self.pc = pc

    def step(self):
        if self.pc == 0:
            self.halt()
            return
        op = self.ram[self.pc % 0x10000]
        self.pc += 1
        if op == 0:
            self.halt()
            return
        opcode = op & 0x1f
        self.s = bool(op & 0x20)
        if op & 0x40:
            self.r = True
            self.src = self.rst
            self.dst = self.wst
        else:
            self.r = False
            self.src = self.wst
            self.dst = self.rst
        self.k = True if op & 0x80 else False
        self.src.keep(self.k) # set keep mode

        if opcode == 0x00:   # LIT
            if op >= 0x80:
                self.push(self.src, self.peek(self.pc))
                self.pc += 2 if self.s else 1
            elif op == 0x20: # JCI
                self.pc -= 1
                if self.wst.pop8():
                    self.pc += self.peek16(self.pc+1) + 3
                    self.pc = self.pc % 0x10000
                else:
                    self.pc += 3
            elif op == 0x40: # JMI
                self.pc -= 1
                self.pc += self.peek16(self.pc+1) + 3
                self.pc &= 0xffff
            elif op == 0x60: # JSI
                self.pc -= 1
                self.rst.push16(self.pc + 2 + 1)
                self.pc += self.peek16(self.pc + 1) + 3
                self.pc &= 0xffff

        elif opcode == 0x01: # INC
            a = self.pop()
            a += 1
            self.push(self.src, a)
        elif opcode == 0x02: # POP
            self.pop()
        elif opcode == 0x03: # NIP
            b = self.pop()
            self.pop()
            self.push(self.src, b)
        elif opcode == 0x04: # SWP
            b, a = self.pop(), self.pop()
            self.push(self.src, b)
            self.push(self.src, a)
        elif opcode == 0x05: # ROT
            c, b, a = self.pop(), self.pop(), self.pop()
            self.push(self.src, b)
            self.push(self.src, c)
            self.push(self.src, a)
        elif opcode == 0x06: # DUP
            a = self.pop()
            self.push(self.src, a)
            self.push(self.src, a)  
        elif opcode == 0x07: # OVR
            b, a = self.pop(), self.pop()
            self.push(self.src, a)
            self.push(self.src, b)
            self.push(self.src, a)
        elif opcode == 0x08: # EQU
            b, a = self.pop(), self.pop()
            self.src.push8(int(a == b))
        elif opcode == 0x09: # NEQ
            b, a = self.pop(), self.pop()
            self.src.push8(int(a!=b))
        elif opcode == 0x0a: # GTH:
            b = self.pop()
            a = self.pop()
            if a > b:
                self.src.push8(0x01)
            else:
                self.src.push8(0x00)
            # b, a = self.pop(), self.pop()
            # self.src.push8(int(a > b))
        elif opcode == 0x0b: # LTH
            b, a = self.pop(), self.pop()
            self.src.push8(a < b)
        elif opcode == 0x0c: # JMP
            a = self.pop()
            self.jump(a)
        elif opcode == 0x0d: # JCN
            a = self.pop()
            flag = self.src.pop8()
            if flag != 0:
                self.jump(a)
        elif opcode == 0x0e: # JSR
            a = self.pop()
            self.dst.push16(self.pc)
            self.jump(a)
        elif opcode == 0x0f: # STH
            a = self.pop()
            self.push(self.dst, a)
        elif opcode == 0x10: # LDZ
            a = self.src.pop8()
            v = self.peek(a)
            self.push(self.src, v)
        elif opcode == 0x11: # STZ
            a, v = self.src.pop8(), self.pop()
            self.poke(a, v)
        elif opcode == 0x12: # LDR
            a = signed(self.src.pop8())
            v = self.peek(self.pc + a)
            self.push(self.src, v)
        elif opcode == 0x13: # STR
            a = signed(self.src.pop8())
            v = self.pop()
            self.poke(self.pc + a, v)
        elif opcode == 0x14: # LDA
            a = self.src.pop16()
            v = self.peek(a)
            self.push(self.src, v)
        elif opcode == 0x15: # STA
            a = self.src.pop16()
            v = self.pop()
            self.poke(a, v)
        elif opcode == 0x16: # DEI
            a = self.src.pop8()
            self.push(self.src, self.devr(a))
        elif opcode == 0x17: # DEO
            a, v = self.src.pop8(), self.pop()
            self.devw(a, v)
        elif opcode == 0x18: # ADD
            b, a = self.pop(), self.pop()
            self.push(self.src, a + b)
        elif opcode == 0x19: # SUB
            b, a = self.pop(), self.pop()
            self.push(self.src, a - b)
        elif opcode == 0x1a: # MUL
            b, a = self.pop(), self.pop()
            self.push(self.src, a * b)
        elif opcode == 0x1b: # DIV
            b, a = self.pop(), self.pop()
            self.push(self.src, a // b)
        elif opcode == 0x1c: # AND
            b, a = self.pop(), self.pop()
            self.push(self.src, a & b)
        elif opcode == 0x1d: # ORA
            b, a = self.pop(), self.pop()
            self.push(self.src, a | b)
        elif opcode == 0x1e: # EOR
            b, a = self.pop(), self.pop()
            self.push(self.src, a ^ b)
        elif opcode == 0x1f: # SFT
            b, a = self.src.pop8(), self.pop()
            left, right = (b & 0xf0) >> 4, b & 0x0f
            self.push(self.src, a >> right << left)
        else:
            print("opcode: ", opcode)

    def push(self, stack, v):
        if self.s:
            stack.push16(v)
        else:
            stack.push8(v)

    def pop(self):
        if self.s:
            return self.src.pop16()
        else:
            return self.src.pop8()

    def peek8(self, addr):
        return self.ram[addr % 10000]

    def peek16(self, addr):
        return (self.ram[addr % 10000] << 8) | (self.ram[(addr+1) % 10000] & 0xff)

    def peek(self, addr):
        if self.s:
            return self.peek16(addr)
        else:
            return self.peek8(addr)

    def poke8(self, addr, v):
        self.ram[addr] = v

    def poke16(self, addr, v):
        self.ram[addr] = (v & 0xff00) >> 8
        self.ram[addr+1] = v & 0xff

    def poke(self, addr, v):
        if self.s:
            self.poke16(addr, v)
        else:
            self.poke8(addr, v)

    def jump(self, a):
        if self.s:
            self.pc = a
        else:
            self.pc += signed(a)

    def devr(self, a):
        v = self.dei(a)
        if self.s:
            v = v << 8 | self.dei((a+1) & 0xff)
        return v

    def devw(self, a, v):
        if self.s:
            self.deo(a, v >> 8)
            self.deo((a + 1) & 0xff, v & 0xff)
        else:
            self.deo(a, v & 0xff)

    def dei(self, a):
        return self.dev[a]

    def deo(self, a, v):
        self.dev[a] = v

    def get_vec(self, a):
        a = a & 0xf0
        return (self.dev[a] << 8) | self.dev[a+1]

    def halt(self):
        self.halted = True

    def __repr__(self):
        res = (f"wst: {self.wst}\n"
               f"rst: {self.rst}\n"
               f"pc: {hex(self.pc)}\n")
        return res

class Varvara(Uxn):
    def __init__(self):
        super().__init__()

        # Set screensize to 240x240
        self.dev[0x22]=0x00
        self.dev[0x23]=240
        self.dev[0x24]=0x00
        self.dev[0x25]=240

        n_bytes = int(240*240*2/8) # 2 bits per pixel
        self.background_array = bytearray(n_bytes)
        self.background = memoryview(self.background_array)
        self.foreground_array = bytearray(n_bytes)
        self.foreground = memoryview(self.foreground_array)
        
    def deo(self, a, v):
        super().deo(a, v)
        if a == 0x18:
            print(chr(v), end='')
        elif a == 0x2e: # .Screen/pixel
            fill = bool(v & 0x80)
            foreground = bool(v & 0x40)
            flip_y = bool(v & 0x20)
            flip_x = bool(v & 0x10)
            color = v & 0x03
            
            x = self.dev[0x29]
            y = self.dev[0x2b]
            red1 = self.dev[0x08]
            red2 = self.dev[0x09]
            green1 = self.dev[0x0a]
            green2 = self.dev[0x0b]
            blue1 = self.dev[0x0c]
            blue2 = self.dev[0x0d]

            if color == 0:
                # TODO: Color 0 means transparency in the foreground layer.
                r = red1 >> 4
                g = green1 >> 4
                b = blue1 >> 4
            elif color == 1:
                r = red1 & 0xf
                g = green1 & 0xf
                b = blue1 & 0xf
            elif color == 2:
                r = red2 >> 4
                g = green2 >> 4
                b = blue2 >> 4
            else:
                r = red2 & 0xf
                g = green2 & 0xf
                b = blue2 & 0xf

            # Change to RGB565
            r = r << 1
            g = g << 2
            b = b << 1
            color565 = r << 11 | g << 5 | b
            auto = self.dev[0x26]
            length = auto >> 4
            auto_addr = bool(auto & 0x04)
            auto_y = auto & 0x02
            auto_x = auto & 0x01
            # TODO: Respect foreground and background layers
            if fill:
                if flip_x:
                    if flip_y:
                        wasp.watch.drawable.fill(color565, 0, 0, w=x, h=y)
                    else:
                        wasp.watch.drawable.fill(color565, 0, y, w=x)
                else:
                    if flip_y:
                        wasp.watch.drawable.fill(color565, x, 0, h=y)
                    else:
                        wasp.watch.drawable.fill(color565, x, y)
            else:
                wasp.watch.drawable.fill(color565, x, y, 1 + auto_x * length , 1 + auto_y * length)
    def devr(self, a):
        if a & 0xf0 == 0xc0:
            day_time = time.localtime()
            self.dev[0xc0] = (day_time[0]) >> 8
            self.dev[0xc1] = day_time[0] & 0xff
            self.dev[0xc2] = day_time[1]
            self.dev[0xc3] = day_time[2]
            self.dev[0xc4] = day_time[3]
            self.dev[0xc5] = day_time[4]
            self.dev[0xc6] = day_time[5]
            self.dev[0xc7] = (day_time[6] + 1) % 7
            self.dev[0xc8] = (day_time[7] - 1) >> 8
            self.dev[0xc9] = (day_time[7] - 1) & 0xff
            self.dev[0xca] = day_time[8]
        return super().devr(a)
        

with open("main.rom", 'rb') as f:
    rom = f.read()

uxn = Varvara()

uxn.load(rom)

print(f"{len(rom)} bytes")
print()

uxn.eval(Uxn.page_program)

screen_vector=uxn.dev[0x20] << 8 |  uxn.dev[0x21]
while screen_vector != 0:
    uxn.eval(screen_vector)
    screen_vector=uxn.dev[0x20] << 8 |  uxn.dev[0x21]


    


